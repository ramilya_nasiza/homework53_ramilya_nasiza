import React from 'react';

import './task.css';

const Task = props => {
  return (
      props.allTasks.map((task, id) => {
        return (
            <div key={id} className='task clearfix'>
              <input type="checkbox" className='check' onChange={(event) => props.checked(event, task.id)}/>
              <p className='task-text'>{task.text}</p>
              <button onClick={() => props.remove(task.id)} className='remove'><i className="fas fa-trash-alt"></i></button>
            </div>
        )
      })
  )
};

export default Task;