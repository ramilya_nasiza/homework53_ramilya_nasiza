import React from 'react';

import './addTaskForm.css';

const AddTaskForm = props => (
    <div className='addTask'>
      <input
          onChange={props.change}
          type="text"
          placeholder='Add a new task'
          value={props.value}
      />
      <button onClick={props.add}>Add</button>
    </div>
);


export default AddTaskForm;