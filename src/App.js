import React, { Component } from 'react';
import AddTaskForm from './components/addtaskform/addTaskForm.js';
// import Task from './components/task/task.js';

import './App.css';
import Task from "./components/task/task";


class App extends Component {

  state = {
    tasks: [
      {text: 'Clean room', id: 1, check: false},
      {text: 'Do homework', id: 2, check: false},
      {text: 'Buy vegetables', id: 3, check: false}
    ],
    text: '',
  };

  changeHandler = (event) => {
    this.setState({
      text: event.target.value
    })
  };

  checkedFunc = (event, id) => {

    const tasks = [...this.state.tasks];
    const index = tasks.findIndex(task => task.id === id);
    tasks[index].check = event.target.checked;

    this.setState({tasks});
  };

  addTask = () => {
    if(this.state.text !== ''){
      const tasks = [...this.state.tasks];
      const oneTask = {text: this.state.text, id: tasks.length + 1};
      tasks.push(oneTask);

      this.setState({tasks: tasks, text: ''});
    } else{
      alert('You write anything');
    }
  };

  removeTask = (id) =>{
    const tasks = [...this.state.tasks];
    const index = tasks.findIndex(task => task.id === id);
    tasks.splice(index, 1);

    this.setState({tasks});
  };

  render() {
    return (
      <div className="App">
        <AddTaskForm
            change={(event) => this.changeHandler(event)}
            add={() => this.addTask()}
            value={this.state.text}
        />
        <Task
            allTasks={this.state.tasks}
            remove={(id) => this.removeTask(id)}
            checked={(event, id) => this.checkedFunc(event, id)}
        />
      </div>
    );
  }
}

export default App;
